#include <stdio.h>

//声明全局变量引用
int count;

//本来此处也应是声明全局变量引用，但是此方法定义处没有定义extern
extern void write_extern();
//当您使用 extern 时，对于无法初始化的变量，会把变量名指向一个之前定义过的存储位置

int main(){
    count=5;
    write_extern();
}