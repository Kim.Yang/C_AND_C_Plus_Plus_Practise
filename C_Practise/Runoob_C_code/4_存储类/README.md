# 储存类

存储类知识点思维导图：

![存储类](https://tva1.sinaimg.cn/large/0082zybpgy1gc34hc7bquj30w60ftn7q.jpg)

个人理解总结：

*   auto是局部变量默认的 存储类，static 是 全局变量默认的存储类
*   static 能让局部变量变成全局变量，但全局变量的作用域仍为同一源码文件中
*   想让全局变量的作用域突破同一文件就需要 extern 类
*   register 类声明变量存储在寄存器中，而非RAM



**若想使用 extern 使变量跨文件，那么编译时就得将文件一起编译，否则就会导致编译失败**

![一起同时编译](https://tva1.sinaimg.cn/large/0082zybpgy1gc36wnnqdtj309m03nq39.jpg)

这也说明 extern 的作用只是在一起编译的时候生效，并不能在编译前或过程中去从未参加编译的文件中获取！

若想在不一起编译，而是在编译前自动获取，那就需要**预处理器**来自动在编译之前自动获取相关需要编译的文件！

![预处理器编译](https://tva1.sinaimg.cn/large/0082zybpgy1gc36ylvrtaj30cf07p3z6.jpg)