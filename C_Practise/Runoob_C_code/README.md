# IDE 折腾记

刚开始的时候，是准备使用 Xcode 写C语言的，但是由于对其实在不熟悉，且其定位于项目开发，对于学习语法，写一些零散小文件的新手着实不太友好，故最后我放弃了它！

然后我准备使用一直在应用程序里面吃灰的 CLion 但是长期没用，我都不知道它的激活码过期了，也懒得再折腾了，就又放弃了

最后，看着最近学习前端使用正顺手的 VScode，于是便折腾起了它。经过短暂的爬贴，两个插件就解决了环境问题。感叹 VScode 无所不能！

![image-20200217214318681](https://tva1.sinaimg.cn/large/0082zybpgy1gbzqd8jornj30qm0qwadp.jpg)



最后晒一晒，学习桌面，是我认为比较高效合理的解决方案～
![image-20200217214602047](https://tva1.sinaimg.cn/large/0082zybpgy1gbzqg1yd85j31h40r215v.jpg)

这是学习[C语言教程](https://www.runoob.com/cprogramming/c-tutorial.html)时，运行的教程中的代码！
