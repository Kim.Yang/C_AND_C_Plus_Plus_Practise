# 变量

对变量部分的内容总结如下：

![变量声明及定义](https://tva1.sinaimg.cn/large/0082zybpgy1gbzu3e3si5j30wu0onail.jpg)

问题留存：

>   不带初始值定义的时候，带有静态储存持续时间的变量会被初始化为NULL。

什么是带有静态存储持续时间的变量？

就是静态变量，也就是用 static 修饰的变量

![image-20200218132101697](https://tva1.sinaimg.cn/large/0082zybpgy1gc0hgwr3guj30jp09bwfh.jpg)