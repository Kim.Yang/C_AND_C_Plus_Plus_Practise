//  Created by kim yang on 2020/2/17.
//  Copyright © 2020 kim yang. All rights reserved.
//
#include <stdio.h>

// 函数外定义变量x和y
int x,y;
int addtwonum(){
    //在函数内部声明变量x和y为外部变量
    extern int x,y;
    // 给外部变量（全局变量）x和y赋值
    x=1;
    y=2;
    return x+y;
}

int main() {
  
    int result = addtwonum();
    printf("result  is:%d",result);
    return 0;
}
