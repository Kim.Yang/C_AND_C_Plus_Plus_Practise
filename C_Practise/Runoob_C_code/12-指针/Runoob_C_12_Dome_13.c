#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int * getRandom(){
//	返回指针的函数关机就在于上面的 *
	static int r[10];
	int i;
	
//	设置种子,相当与设置一个起点。
	srand((unsigned)time(NULL));	
//	srand 函数是随机数发生器的初始化函数
//	time 函数可以获取当前系统时间
	for(i=0;i<10;i++){
		r[i]=rand();
//		rand() 每一次调用时都会查看之前是否有srand函数
//		如果有就会调用srand来初始化，如果没有，默认使用srand(1)来初始化;
		printf("%d\n",r[i]);
	}
	return r;
}

int main(int argc, char *argv[]) {
//	一个指向整数的指针
	int *p;
	int i;
	
	p=getRandom();
	for(i=0;i<10;i++){
		printf("*(p+[%d]):%d\n",i,*p+i);
	}
	return 0;
}