#include<stdio.h>

//使用#define预处理器定义常量
#define LENGTH 10
#define WIDTH 5
#define NEWLINE '\n'
//注意预处理器不用分号分割

int main(){ 
    int area;
    area = LENGTH*WIDTH;
    printf("Value of area : %d",area);
    printf("%c",NEWLINE);
    return 0;

}