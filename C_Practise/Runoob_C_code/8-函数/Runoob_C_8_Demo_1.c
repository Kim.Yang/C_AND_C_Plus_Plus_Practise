#include <stdio.h>

//	函数声明
int max(int,int);

int main(int argc, char *argv[]) {

//		局部变量定义
		int a=100;
		int b=200;
		int ret;
		
//		调用函数来获取最大值
		ret=max(a,b);
		printf("最大值是:%d",ret);
		return 0;
	
}
// 定义取大值的函数
int max(int num1,int num2 ){
	if(num1>num2)
		return num1;
	else
	return num2;
}