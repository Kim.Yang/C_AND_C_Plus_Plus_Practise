# Leetcode

在Clion 中刷Leetcode题目，可以使用一个插件，叫做 Leetcode editer

自定义设定如下：

![image-20200630225713964](https://tva1.sinaimg.cn/large/007S8ZIlly1ggapjf8ol9j31bl0u0qfj.jpg)

CodeFileName:

```LT_${question.frontendQuestionId}_$!velocityTool.camelCaseName(${question.titleSlug})```

CodeTemplate:
```
// ${question.frontendQuestionId} ${question.title}
// ${question.titleSlug}

${question.content}

${question.code}
```

做如上自定义是为了将默认以中文命名的文件方式改为以英文名，以方便`CmakeList.txt`文件在Clion中编译。但是后来发现运行测试用例不用在本地，Leetcode Editor可以直接将代码上传至LeetCode服务器上运行然后拉取返回结果～

![image-20200630225616706](https://tva1.sinaimg.cn/large/007S8ZIlly1ggapiisyb1j30ko0hgtcw.jpg)

所以最后，为了方便查看文件结构，就恢复了默认命名方式～

## 鉴于最近处于筹备考研的特殊时期，故leetcode刷题将会搁置一段时间，等待这一阶段过去之后将会全身心投入其中！