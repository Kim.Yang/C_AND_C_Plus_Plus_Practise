// 44 通配符匹配
// wildcard-matching

//给定一个字符串 (s) 和一个字符模式 (p) ，实现一个支持 '?' 和 '*' 的通配符匹配。 
//
// '?' 可以匹配任何单个字符。
//'*' 可以匹配任意字符串（包括空字符串）。
// 
//
// 两个字符串完全匹配才算匹配成功。 
//
// 说明: 
//
// 
// s 可能为空，且只包含从 a-z 的小写字母。 
// p 可能为空，且只包含从 a-z 的小写字母，以及字符 ? 和 *。 
// 
//
// 示例 1: 
//
// 输入:
//s = "aa"
//p = "a"
//输出: false
//解释: "a" 无法匹配 "aa" 整个字符串。 
//
// 示例 2: 
//
// 输入:
//s = "aa"
//p = "*"
//输出: true
//解释: '*' 可以匹配任意字符串。
// 
//
// 示例 3: 
//
// 输入:
//s = "cb"
//p = "?a"
//输出: false
//解释: '?' 可以匹配 'c', 但第二个 'a' 无法匹配 'b'。
// 
//
// 示例 4: 
//
// 输入:
//s = "adceb"
//p = "*a*b"
//输出: true
//解释: 第一个 '*' 可以匹配空字符串, 第二个 '*' 可以匹配字符串 "dce".
// 
//
// 示例 5: 
//
// 输入:
//s = "acdcb"
//p = "a*c?b"
//输出: false 
// Related Topics 贪心算法 字符串 动态规划 回溯算法


//leetcode submit region begin(Prohibit modification and deletion)


bool isMatch(char * s, char * p){
    /*
     * s为行，p为列
     * 状态方程： case1: if "?", dp[i][j] = dp[i - 1][j - 1] 左上角传递给右下角
     *            case2: if "*", dp[i][j] = dp[i][j - 1] 左下角传递给右下角，作为空字符
     *                           dp[i][j] = dp[i - 1][j] 右上角传递给右下角，作为任意字符
     * 初始条件： dp[0][0] = true;
     * 边界条件： dp[0][j] = false; dp[i][0] = dp[i - 1][0]
     */

    if (NULL == s || NULL == p)
    {
        return false;
    }

    int lenOfS = strlen(s);
    int lenOfP = strlen(p);
    int row = 0;
    int col = 0;

    bool** ppbIsMatch = (bool**)malloc((lenOfP + 1) * sizeof(bool*));
    for (row = 0; row <= lenOfP; row++)
    {
        ppbIsMatch[row] = (bool*)malloc((lenOfS + 1) * sizeof(bool));
        memset(ppbIsMatch[row], false, (lenOfS + 1) * sizeof(bool));
    }

    ppbIsMatch[0][0] = true;

    for (row = 1; row <= lenOfP; row++)
    {
        if ('*' == *(p + row - 1))
        {
            ppbIsMatch[row][0] = ppbIsMatch[row - 1][0];
        }
    }

    for (row = 1; row <= lenOfP; row++)
    {
        for (col = 1; col <= lenOfS; col++)
        {
            if (*(s + col - 1) == *(p + row - 1) || '?' == *(p + row - 1))
            {
                ppbIsMatch[row][col] = ppbIsMatch[row - 1][col - 1];
            }
            else if ('*' == *(p + row - 1))
            {
                ppbIsMatch[row][col] = ppbIsMatch[row - 1][col] || ppbIsMatch[row][col - 1];
            }
        }
    }

    return ppbIsMatch[lenOfP][lenOfS];
}

//leetcode submit region end(Prohibit modification and deletion)
