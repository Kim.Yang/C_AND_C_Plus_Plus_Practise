// 718 最长重复子数组
// maximum-length-of-repeated-subarray

//给两个整数数组 A 和 B ，返回两个数组中公共的、长度最长的子数组的长度。 
//
// 示例 1: 
//
// 
//输入:
//A: [1,2,3,2,1]
//B: [3,2,1,4,7]
//输出: 3
//解释: 
//长度最长的公共子数组是 [3, 2, 1]。
// 
//
// 说明: 
//
// 
// 1 <= len(A), len(B) <= 1000 
// 0 <= A[i], B[i] < 100 
// 
// Related Topics 数组 哈希表 二分查找 动态规划


//leetcode submit region begin(Prohibit modification and deletion)


int findLength(int* A, int ASize, int* B, int BSize){
    int dp[ASize][BSize];
    //在内存中填充某个给定的值，它对较大的结构体或数组进行清零操作的一种最快的方法。
    memset(dp, 0, sizeof(int) * ASize * BSize);
    int i, j;
    int ret = 0;

    for (i = 0; i < ASize; i++) {
        if (A[i] == B[0]) {
            dp[i][0] = 1;
            ret = 1;
        }
    }

    for (i = 0; i < BSize; i++) {
        if (B[i] == A[0]) {
            dp[0][i] = 1;
            ret = 1;
        }
    }

    for (i = 1; i < ASize; i++) {
        for (j = 1; j < BSize; j++) {
            if (A[i] == B[j]) {
                dp[i][j] = dp[i - 1][j - 1] + 1;
                //printf("%d %d %d\n",i,j,dp[i][j]);
                ret = (dp[i][j] > ret) ? dp[i][j] : ret;
            }
        }
    }
    return ret;
}

//leetcode submit region end(Prohibit modification and deletion)
