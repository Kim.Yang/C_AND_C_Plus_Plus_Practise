// 209 长度最小的子数组
// minimum-size-subarray-sum

//给定一个含有 n 个正整数的数组和一个正整数 s ，找出该数组中满足其和 ≥ s 的长度最小的连续子数组，并返回其长度。如果不存在符合条件的连续子数组，返回
// 0。 
//
// 
//
// 示例： 
//
// 输入：s = 7, nums = [2,3,1,2,4,3]
//输出：2
//解释：子数组 [4,3] 是该条件下的长度最小的连续子数组。
// 
//
// 
//
// 进阶： 
//
// 
// 如果你已经完成了 O(n) 时间复杂度的解法, 请尝试 O(n log n) 时间复杂度的解法。 
// 
// Related Topics 数组 双指针 二分查找


//leetcode submit region begin(Prohibit modification and deletion)

#define MIN(a,b)            (((a) < (b)) ? (a) : (b))
int minSubArrayLen(int s, int* nums, int numsSize){
    int sum=0,ans=0x7fFfFfFf;   //用int上限值，常量速度快一丢丢。
    for(int i=0,j=0; j<numsSize ;){
        while(j<numsSize && sum<s) sum += nums[j++];   //j增加直到达标
        while(/*i<j &&*/ sum>=s) sum -= nums[i++];  //i追赶直到再次不达标
        ans=MIN(ans,j-i+1);   //注意此时的sum(i,j)<s，而sum(i-1,j)才>=s，故取j-(i-1)==j-i+1
    }
    return (ans>numsSize)?0:ans;   //ans初始设为numsSize+1，若无更新则ans>numsSize返回0。
}

//leetcode submit region end(Prohibit modification and deletion)
