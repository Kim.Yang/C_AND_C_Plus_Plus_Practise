// 面试题 16.11 跳水板
// diving-board-lcci

//你正在使用一堆木板建造跳水板。有两种类型的木板，其中长度较短的木板长度为shorter，长度较长的木板长度为longer。你必须正好使用k块木板。编写一个方
//法，生成跳水板所有可能的长度。 
// 返回的长度需要从小到大排列。 
// 示例： 
// 输入：
//shorter = 1
//longer = 2
//k = 3
//输出： {3,4,5,6}
// 
// 提示： 
// 
// 0 < shorter <= longer 
// 0 <= k <= 100000 
// 
// Related Topics 递归 记忆化 
// 👍 51 👎 0


//leetcode submit region begin(Prohibit modification and deletion)


/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
int* divingBoard(int shorter, int longer, int k, int* returnSize){
    if(k==0){
        *returnSize=0;
        return NULL;
    }
    if(shorter==longer){
        int* ret=(int *)malloc(sizeof(int));
        ret[0]=k*longer;
        *returnSize=1;
        return ret;
    } else{
        int *ret=(int *)malloc((k+1)*sizeof(int));
        for(int i=0;i<=k;i++){
            ret[i]=(i*longer)+(k-i)*shorter;
            //这里longer和shorter的顺序不能互换？
        }
        *returnSize=k+1;
        return ret;
    }
    return NULL;
}


//leetcode submit region end(Prohibit modification and deletion)
